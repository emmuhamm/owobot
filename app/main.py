from typing import Union
from owotext import OwO
from fastapi import FastAPI
import os
from pydantic import BaseModel

uwu = OwO()
app = FastAPI()

class MattermostRequest(BaseModel):
    text: str 
    token: str 
    channel_id: Union[str, None] = None
    channel_name: Union[str, None] = None
    team_domain: Union[str, None] = None
    team_id: Union[str, None] = None
    post_id: Union[str, None] = None
    timestamp: Union[str, None] = None
    trigger_word: Union[str, None] = None
    user_id: Union[str, None] = None
    user_name: Union[str, None] = None

def check_token(token):
    webhook_token = os.environ["MATTERMOST_TOKEN"]
    if token == webhook_token:
        return True
    return False

def sanitise(text: str):
    text = text.replace("#owo","",1) 
    text = text.replace("#", "")
    return text

@app.get("/")
def read_root():
    return {"Hello!!": "World"}

@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}

@app.get("/something/{owo}")
def parse(owo: str):
    ret = uwu.whatsthis(owo)
    return ret

@app.post("/mattermost/")
def do_mattermost(body: MattermostRequest):
    if not check_token(body.token):
        return {"text": "You are not authorised!"}

    test_text = sanitise(body.text)
    ret = uwu.whatsthis(test_text)
    return {"text": ret}
