# 
FROM python:3.9

# 
WORKDIR /code

# 
COPY ./requirements.txt /code/requirements.txt

# 
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

# 
COPY ./app /code/app

#
ENV DOCKERFILEVAR=value2
RUN echo "the ENV variable value is $DOCKERFILEVAR"

ENV PAASVAR=empty
RUN echo "the ENV variable value is $PAASVAR"

ENV MATTERMOST_TOKEN=empty
RUN echo "the ENV variable value is $MATTERMOST_TOKEN"

# 
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8080"]