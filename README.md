# OwoBot

## Running Locally

1. Build image

```
docker build -t myimage . 
```

2. Start Docker container

```
docker run -d --name mycontainer -p 8080:8080 myimage
```

3. View api

go to http://localhost:8080/docs


## Viewing on a website (!)

It's currently available at https://owobotv2-emuapi.app.cern.ch/docs, however only accessible from the CERN intranet. This is accessible via sshuttle from anywhere in the world using sshuttle. [See the ssh tunneling guide](https://security.web.cern.ch/recommendations/en/ssh_tunneling.shtml).

If its a 503 error, then it means that the pods are down because I don't want to keep it public unless I'm testing this. If you have access to the PAAS page (you know who you are) you can add a pod there to test it out. 

### Adding a pod
1. Go to paas.cern.ch
2. Go go the correct project
3. Make sure you are in developer mode, not administrator mode
4. In topology, select owobotv2, go to details, and press the up arrow to scale it up to 1. 
5. When finish, scale it back down to 0.